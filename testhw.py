import os
from tests.testtools import BaseTester, Runner, NotebookError, STREAM

__unittest = True

def test_results(problem):
    if problem == 1:
        Runner.test_output(problem, Problem1R)
    elif problem == 2:
        Runner.test_output(problem, Problem2R)
    elif problem == 3:
        Runner.test_output(3, Problem3R)

def test_notebook(problem):
    if problem == 1:
        Runner.run_problem(problem, [Problem1])
    elif problem == '2A':
        Runner.run_problem(2, [Problem2Full])
    elif problem == '2B':
        Runner.run_problem(2, [Problem2Clipped])
    elif problem == '3A':
        Runner.run_problem(3, [Problem3Full])
    elif problem == '3B':
        Runner.run_problem(3, [Problem3Clipped])


class Problem1(BaseTester):
    problem = 1
    args = {}
    input_filename = 'aster_clipped.tif'
    result_filename = 'test_points.shp'
    expected_filename = 'p1_test.csv'

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            raster_filename=self.input_filename,
            shape_filename=self.result_filename,
        )
        self.expected_path = os.path.join(self.test_folder, 'data', self.expected_filename)
        self.result_path = os.path.join(self.data_folder, self.result_filename)

    def test_columns(self):
        """Test column names"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        fields = set([x.name.lower() for x in arcpy.ListFields(self.result_path)])
        missing = set(['red', 'green', 'nir']) - fields
        self.assertEqual(len(missing), 0, f'Shapefile is missing columns: {", ".join(missing)}')

    def test_data(self):
        """Test data values"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        import pandas as pd
        fields = ['id', 'green', 'red', 'nir']
        expected_df = pd.read_csv(self.expected_path).set_index('id')
        with arcpy.da.SearchCursor(self.result_path, fields) as rows:
            result_df = pd.DataFrame(data=list(rows), columns=fields).set_index('id')
        self.assertTrue(result_df.equals(expected_df), f'Output data appears to be incorrect')


class Problem1R(Problem1):
    result_filename = 'sites.shp'
    expected_filename = 'p1.csv'


class RasterTester(BaseTester):
    args = {}
    stats = {}

    def test_rows(self):
        """Test row count"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        result = arcpy.Raster(self.result_path).height
        expected = self.stats['rows']
        self.assertEqual(result, expected, f'Output raster has {result} rows instead of {expected}')

    def test_columns(self):
        """Test column count"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        result = arcpy.Raster(self.result_path).width
        expected = self.stats['columns']
        self.assertEqual(result, expected, f'Output raster has {result} columns instead of {expected}')

    def test_bands(self):
        """Test band count"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        result = arcpy.Raster(self.result_path).bandCount
        expected = self.stats['bands']
        self.assertEqual(result, expected, f'Output raster has {result} bands instead of {expected}')

    def test_min(self):
        """Test minimum pixel value"""
        self._test_stat('min')

    def test_max(self):
        """Test maximum pixel value"""
        self._test_stat('max')

    def test_mean(self):
        """Test mean pixel value"""
        self._test_stat('mean')

    def test_std(self):
        """Test standard deviation of pixel values"""
        self._test_stat('standardDeviation')

    def test_nodata(self):
        """Test number of NoData pixels"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        result = arcpy.Raster(self.result_path)
        count = arcpy.RasterToNumPyArray(result == result.noDataValue).sum()
        expected = self.stats['nodata']
        self.assertEqual(count, expected, f'Output raster has {count:,} NoData pixels instead of {expected:,}')

    def _test_stat(self, name):
        self.notebook_ran()
        self.result_exists()
        import arcpy
        self.assertEqual(
            round(arcpy.Raster(self.result_path).getStatistics()[0][name], 4),
            round(self.stats[name], 4),
            f'Incorrect {name} pixel value',
        )


class Problem2Full(RasterTester):
    problem = 2
    suffix = 'a'
    input_filename = 'aster.img'
    result_filename = 'savi_test.tif'
    L = 0.5
    stats = dict(
        min=-1.496021270752,
        max=1.4942084550858,
        mean=0.077014790752435,
        standardDeviation=0.23773409873184,
        bands=1,
        nodata=2739877080,
        rows=5033,
        columns=5665,
    )

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            input_raster=os.path.join(self.repo_folder, 'data', self.input_filename),
            output_raster=self.result_filename,
            L=self.L,
        )
        self.result_path = os.path.join(self.data_folder, self.result_filename)


class Problem2Clipped(RasterTester):
    problem = 2
    suffix = 'b'
    input_filename = 'aster_clipped.tif'
    result_filename = 'savi_test.tif'
    L = 0.7
    stats = dict(
        min=-1.6912949085236,
        max=1.1971831321716,
        mean=0.16374203365659,
        standardDeviation=0.33818771604462,
        bands=1,
        nodata=64380360,
        rows=930,
        columns=987,
    )

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            input_raster=self.input_filename,
            output_raster=self.result_filename,
            L=self.L,
        )
        self.result_path = os.path.join(self.data_folder, self.result_filename)


class Problem2R(Problem2Full):
    result_filename = 'savi.tif'


class Problem3(RasterTester):
    problem = 3
    input_filename = ''
    result_filename = ''
    kernel_filename = ''
    kernel_path = ''

    def notebook_ran(self):
        if self.use_test_folder and not self.notebook_exists:
            self.skipTest('Notebook not found')
        if self.use_test_folder and not os.path.exists(self.kernel_path):
            self.skipTest('Kernel file not found')

    def test_notebook(self, args=None):
        """Run notebook"""
        STREAM.write(self.get_args())
        self.copy_data()
        if not self.notebook_exists:
            self.skipTest(self.in_notebook + ' not found!')
        if not os.path.exists(self.kernel_path):
            self.skipTest(self.kernel_path + ' not found!')
        result = self.execute_notebook()
        if result:
            raise NotebookError(result)


class Problem3Full(Problem3):
    suffix = 'a'
    input_filename = 'aster.img\ASTER_Band1'
    result_filename = 'sharp_test.tif'
    kernel_filename = 'sharpen.txt'
    stats = dict(
        min=-734.0,
        max=799.0,
        mean=0.0,
        standardDeviation=14.610226688671,
        bands=1,
        nodata=0,
        rows=5033,
        columns=5665,
    )

    def setUp(self):
        super().setUp()
        self.result_path = os.path.join(self.data_folder, self.result_filename)
        self.kernel_path = os.path.join(self.repo_folder, self.kernel_filename)
        self.args = dict(
            input_raster=os.path.join(self.repo_folder, 'data', self.input_filename),
            output_raster=self.result_path,
            kernel_filename = self.kernel_path,
        )


class Problem3Clipped(Problem3):
    suffix = 'b'
    input_filename = 'aster_clipped.tif\Band_1'
    result_filename = 'sharp_test.tif'
    kernel_filename = 'test_kernel.txt'
    stats = dict(
        min=-298.0,
        max=1038.0,
        mean=251.35509799436,
        standardDeviation=166.59305191192,
        bands=1,
        nodata=0,
        rows=930,
        columns=987,
    )

    def setUp(self):
        super().setUp()
        self.result_path = os.path.join(self.data_folder, self.result_filename)
        self.kernel_path = os.path.join(self.data_folder, self.kernel_filename)
        self.args = dict(
            input_raster=os.path.join(self.data_folder, self.input_filename),
            output_raster=self.result_path,
            kernel_filename=self.kernel_path,
        )

class Problem3R(Problem3Full):
    result_filename = 'sharpened_aster.tif'
