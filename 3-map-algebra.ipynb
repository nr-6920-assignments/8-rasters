{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 8-rasters\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\8-rasters\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import arcpy\n",
    "\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True\n",
    "\n",
    "arcpy.CheckOutExtension('spatial')\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you're running this in ArcGIS Pro\n",
    "\n",
    "Make sure the ASTER map is the active map because it has the data used in this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Memory management\n",
    "\n",
    "Some of you might have problems with this notebook because of the amount of RAM used, so we're going to limit the memory usage by deleting unneeded variables as we go along. To do this, we'll use the garbage collection module called `gc`. When you call `gc.collect()`, it cleans up objects that aren't needed anymore and frees up the memory they were using. In order for it to do much good, though, you need to use `del` to delete things when you're done with them.\n",
    "\n",
    "We'll also start saving the results of `imshow()` into a variable so that it can be explicitly deleted and garbage collected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import gc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Map algebra\n",
    "\n",
    "The [Raster Calculator](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/raster-calculator.htm) is a common way to manipulate raster data using ArcGIS. This notebook will show you how to do the same stuff, but with Python.\n",
    "\n",
    "So let's do something you could do with Raster Calculator, but in a script. We're going to calculate a [Normalized Difference Vegetation Index](https://en.wikipedia.org/wiki/Normalized_Difference_Vegetation_Index) (NDVI) on an [ASTER](https://en.wikipedia.org/wiki/Advanced_Spaceborne_Thermal_Emission_and_Reflection_Radiometer) satellite image. NDVI is basically a ratio between red and near-infrared wavelengths collected by the sensor. Plants absorb red light and use it for photosynthesis, but they reflect near-infrared, so you can use the ratio of the two as a rough indicator of active photosynthesis, and thus, healthy vegetation. Here's the equation:\n",
    "\n",
    "\\begin{equation*}\n",
    "NDVI = \\frac{NIR - RED}{NIR + RED}\n",
    "\\end{equation*}\n",
    "\n",
    "This formula returns a number between -1 and 1, with values closer to 1 meaning more photosynthesis is occurring.\n",
    "\n",
    "There are three bands in the ASTER image. The second band is red and the third band is near-infrared (NIR).\n",
    "\n",
    "To do the calculation, you need to create two raster objects, one for red and one for NIR."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Single-band rasters\n",
    "\n",
    "Let's start by using single band images. The second (red) band is in a file called aster_b2.tif and the third (NIR) band is in a file called aster_b3.tif."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Raster object from the red (second) band.\n",
    "red = arcpy.Raster('aster_b2.tif')\n",
    "\n",
    "# Create a Raster object from the near-infrared (third) band.\n",
    "nir = arcpy.Raster('aster_b3.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use the `red` and `nir` variables do do some raster math.\n",
    "\n",
    "*(This might take several seconds to run.)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the NDVI.\n",
    "ndvi = (nir - red) / (nir + red)\n",
    "\n",
    "# Print out statistics for the NDVI raster.\n",
    "print('Min:', ndvi.minimum)\n",
    "print('Max', ndvi.maximum)\n",
    "print('Mean', ndvi.mean)\n",
    "print('Standard deviation', ndvi.standardDeviation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see what it looks like, using the built-in method and matplotlib for those of you who can't use the built-in one. The `Normalize` part of the matplotlib code makes it scale the color ramp between -1 and 1; otherwise it would draw as just two colors (a side effect of the pixel values being small floating point numbers). *This also might take a while to run.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(ndvi)\n",
    "\n",
    "# Draw the NDVI as grayscale.\n",
    "img = plt.imshow(arcpy.RasterToNumPyArray(ndvi), norm=plt.Normalize(-1, 1), cmap='gray')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll clean up the memory like mentioned earlier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Clean up memory\n",
    "del img, red, nir\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python 2.7 math\n",
    "\n",
    "If you find yourself doing this with ArcMap instead of ArcGIS Pro, you need to be aware that Python 2.7 handles division a little differently than you'd expect. If you divide two integers then the result is an integer, even if the real answer isn't an integer. For example, according to Python 2.7, `5 / 2 = 2` but we know that really `5 / 2 = 2.5`. In order to get floating point answers with Python 2.7, you need to convert either the numerator or denominator (or both) to floating point. So to make the previous example work, you'd do something like `5.0 / 2.0`, and then you'd get the expected answer of `2.5`.\n",
    "\n",
    "You don't need to worry about this with ArcGIS Pro and Python 3, but you **absolutely must** worry about it with ArcMap and Python 2.7, which is why I'm telling you. Anyway, in order to make it work with ArcMap, you could either convert part of the equation to floating point, like this:\n",
    "\n",
    "```python\n",
    "ndvi = arcpy.sa.Float(nir - red) / (nir + red)\n",
    "```\n",
    "\n",
    "Or you can use this next snippet to tell Python to use the same type of division that Python 3 uses. Just make sure you do this at the beginning of your notebook or script so that it applies to all of your math.\n",
    "\n",
    "```python\n",
    "from __future__ import division\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building pyramids\n",
    "\n",
    "Let's save the NDVI output and build pyramids using the [Build Pyramids](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/build-pyramids.htm) tool. If you've worked with raster data, you know that the pyramids aren't necessary, but they do allow the image to draw faster in ArcGIS. We'll use bilinear interpolation because the NDVI raster is continuous data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the NDVI raster.\n",
    "ndvi.save('ndvi.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With an older version of ArcGIS I would sometimes get a ton of errors when I tried to run the Build Pyramids tool, although it seemed like it worked anyway. That's not happening to me now, but if it happens to you then I guess you can probably ignore it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build pyramid layers for the NDVI.\n",
    "arcpy.BuildPyramids_management('ndvi.tif', resample_technique='BILINEAR')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nicer plotting\n",
    "\n",
    "ArcGIS automatically stretches raster data when displaying it (unless you've turned this behavior off in the options), which makes the images look a lot nicer. You can do the same thing when plotting with `imshow()` by taking advantage of the `norm` option. Instead of normalizing between -1 and 1, normalize between the mean +/- 2 standard deviations, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'd have to read in the data if we didn't already have it in memory.\n",
    "# ndvi = arcpy.Raster('ndvi.tif')\n",
    "\n",
    "# Figure out the bounds that are two standard deviations from the mean.\n",
    "stretch_min = ndvi.mean - 2 * ndvi.standardDeviation\n",
    "stretch_max = ndvi.mean + 2 * ndvi.standardDeviation\n",
    "\n",
    "# Create a normalizer that matplotlib will use to stretch between our min and max values.\n",
    "stretch = plt.Normalize(stretch_min, stretch_max)\n",
    "\n",
    "# Draw the raster using the normalizer and a gray colormap.\n",
    "img = plt.imshow(arcpy.RasterToNumPyArray(ndvi), norm=stretch, cmap='gray')\n",
    "plt.show()\n",
    "\n",
    "# Clear out memory.\n",
    "del img\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use a numpy [masked array](https://numpy.org/doc/stable/reference/maskedarray.generic.html) to get rid of the black around the edges. Basically this is a way of telling numpy (and thus, matplotlib) to ignore certain values. The pixel values for all three ASTER bands are 0 in those outside black pixels, so there was a zero division error when doing the calculation (because the denominator is NIR + RED, which would be 0 + 0). ArcGIS automatically puts NoData in these error pixels, so we'll use [masked_equal](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ma.masked_equal.html#numpy.ma.masked_equal) to mask out the pixels that are equal to the raster's NoData value. Then we can plot with the standard deviation stretch again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the raster into a numpy array so we can mask it.\n",
    "ndvi_array = arcpy.RasterToNumPyArray(ndvi)\n",
    "\n",
    "# Mask the array using the NoData value from the ndvi raster.\n",
    "# Every pixel that is equal to the NoData value is masked out.\n",
    "masked_ndvi = np.ma.masked_equal(ndvi_array, ndvi.noDataValue)\n",
    "\n",
    "# Plot the masked array.\n",
    "img = plt.imshow(masked_ndvi, norm=stretch, cmap='gray')\n",
    "plt.show()\n",
    "\n",
    "del img\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And if you want to use a different color scheme, you can do that, too. The available schemes are listed [here](https://matplotlib.org/stable/gallery/color/colormap_reference.html) (they're case-sensitive)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img = plt.imshow(masked_ndvi, norm=stretch, cmap='BrBG')\n",
    "plt.show()\n",
    "\n",
    "del img\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also lose the axis or make the plot bigger."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use figsize to set width and height.\n",
    "plt.figure(figsize=(10, 10))\n",
    "\n",
    "# Turn the axis bars off.\n",
    "plt.axis('off')\n",
    "\n",
    "# Plot the image.\n",
    "img = plt.imshow(masked_ndvi, norm=stretch, cmap='BrBG')\n",
    "plt.show()\n",
    "\n",
    "del img\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also export the image if you want, but you need all of the code to draw the plot in the same code cell as the call to `savefig()`. Go look in your data folder after you run this and you'll see the ndvi.png file. You can use other formats, too, including pdf."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 10))\n",
    "plt.axis('off')\n",
    "img = plt.imshow(masked_ndvi, norm=stretch, cmap='BrBG')\n",
    "\n",
    "# Save the figure to a png.\n",
    "plt.savefig(os.path.join(data_folder, 'ndvi.png'))\n",
    "plt.show()\n",
    "\n",
    "del img\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get rid of the `ndvi` and `masked_ndvi` arrays now in order to clear up some more RAM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del ndvi, masked_ndvi\n",
    "gc.collect()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiple-band rasters (like ASTER)\n",
    "\n",
    "So far you've only seen how to access a single band from a raster. If there is more than one band and you want a specific one, then you need to know the band name in order to access it. Lots of times the bands are just called something like 'band_1', but not always, so you need to find out what the names are.\n",
    "\n",
    "The best way to get the list of names is to use [Describe](https://pro.arcgis.com/en/pro-app/arcpy/functions/describe.htm) and then use the [children](https://pro.arcgis.com/en/pro-app/arcpy/functions/describe-object-properties.htm) property, which gives a list of more Describe objects, one for each band. You can see their names with the `name` property. Let's see what bands live in the [ASTER](https://en.wikipedia.org/wiki/Advanced_Spaceborne_Thermal_Emission_and_Reflection_Radiometer) satellite image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a list of Describe objects, one for each band in aster.img.\n",
    "bands = arcpy.Describe('aster.img').children\n",
    "\n",
    "# Loop through the list of Describe objects and print out the band names.\n",
    "for band in bands:\n",
    "    print(band.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now to access one of these bands, you need to combine the filename and band name, like `filename/bandname`. The simplest way is to use the `catalogPath` property for the band, which will give you the entire path for the band.\n",
    "\n",
    "*This is interesting. When I ran this cell in Jupyter I got full paths (which is what I expected). But when I ran it in ArcGIS, I got paths relative to the project directory and prefixed with `/RD=`. So `D:\\classes\\NR6920\\Assignments\\8-rasters\\data\\aster.img\\ASTER_Band1` vs. `/RD=aster.img\\ASTER_Band1`. And even more interesting is that ArcPy didn't know how to use them, even though the documentation says \"everything in ArcGIS knows how to deal with catalog paths'. Google didn't help me out, either, so I'll show those of you using ArcGIS how to make it work when you need it.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loop through the list of Describe objects and print out the full path to each band.\n",
    "for band in bands:\n",
    "    print(band.catalogPath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, this gets the second band in the image (because using an index of 1 gets the second thing from the `bands` list, and then its `catalogPath` is used to get the band object):\n",
    "\n",
    "**If you're using ArcGIS**, comment out the first line and uncomment the second. It gets rid of the `/RD=` at the beginning of the catalogPath and then things work-- or least they did for me!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Raster object from the second band in aster.img.\n",
    "band2 = arcpy.Raster(bands[1].catalogPath) # Jupyter\n",
    "# band2 = arcpy.Raster(bands[1].catalogPath.replace('/RD=', '')) # ArcGIS\n",
    "\n",
    "# Print the band's name.\n",
    "band2.name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have the band, you can treat it exactly the same as you did a single-band raster. For example, let's get the numbers of rows and columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'rows: {band2.height}\\ncolumns: {band2.width}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use this method to get the red and NIR bands from the 3-band aster.img *instead* of from the single-band rasters you used earlier. **Notice that I included the `replace()` that ArcGIS users need. You don't need that if you're using Jupyter, but it doesn't hurt anything.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the list of bands from aster.img.\n",
    "bands = arcpy.Describe('aster.img').children\n",
    "\n",
    "# Create a Raster object from the red (second) band.\n",
    "red = arcpy.Raster(bands[1].catalogPath.replace('/RD=', ''))\n",
    "\n",
    "# Create a Raster object from the near-infrared (third) band.\n",
    "nir = arcpy.Raster(bands[2].catalogPath.replace('/RD=', ''))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use these two bands to calculate NDVI, just like you did earlier with the single-band images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the NDVI.\n",
    "ndvi = (nir - red) / (nir + red)\n",
    "\n",
    "# Print out statistics for the NDVI raster.\n",
    "print('Min:', ndvi.minimum)\n",
    "print('Max', ndvi.maximum)\n",
    "print('Mean', ndvi.mean)\n",
    "print('Standard deviation', ndvi.standardDeviation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And see what it looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Built-in method.\n",
    "display(ndvi)\n",
    "\n",
    "# Matplotlib method.\n",
    "# Get the stretch info.\n",
    "stretch_min = ndvi.mean - 2 * ndvi.standardDeviation\n",
    "stretch_max = ndvi.mean + 2 * ndvi.standardDeviation\n",
    "stretch = plt.Normalize(stretch_min, stretch_max)\n",
    "\n",
    "# Mask out the NoData pixels.\n",
    "ndvi_array = arcpy.RasterToNumPyArray(ndvi)\n",
    "masked_ndvi = np.ma.masked_equal(ndvi_array, ndvi.noDataValue)\n",
    "\n",
    "# Plot the masked array.\n",
    "img = plt.imshow(masked_ndvi, norm=stretch, cmap='gray')\n",
    "plt.show()\n",
    "\n",
    "# Free up memory.\n",
    "del img\n",
    "gc.collect()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "286px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
