# Rasters homework

*Due Monday, March 22, at midnight*

## Notebook comprehension questions

*Worth 10 points*

Work through the notebooks in this order:

- 1-rasters-intro (4 points)
- 2-raster-geoprocessing (3 points)
- 3-map-algebra (1 point)
- 4-moving-windows (2 points)


## Script problems

*Worth 10 points each*

I'm having you set a folder and workspace in some of the problems, and use full paths in others. I use both methods in my work, depending on which one seems more appropriate at the time. For example, if I know all of my files will be in the same folder and I want to save some typing, then I'll probably set a workspace. If the code is accessing files from all over, then I use full paths.

## To get full credit for this week's problems

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

## Turn in

1. problem1.ipynb
2. problem2.ipynb
3. problem3.ipynb
4. sharpen.txt


## Problem 1

Create a notebook called `problem1`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\8-rasters\data'
raster_filename = 'aster.img'
shape_filename = 'sites.shp'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

1. Have your notebook sample the three bands of the raster in `raster_filename` at the points in the `shape_filename` and store the results in the shapefile's attribute table, but don't use the default column names. Do not create a new shapefile.
2. Have the data put into fields called 'green', 'red', and 'nir' instead of using the default names (the bands are in green, red, nir order in aster.img). It's okay to assume that the raster will have three bands and that they'll be in this order, but you don't want to assume that the band names are the same as the ones in aster.img. **See the hints below.**
3. Print out the shapefile's attribute table. Here's a handy little code snippet that will display the attribute table as a nicely formatted pandas dataframe. Just adapt it to use the correct filename variable, and you can change the fields it displays if you want it to match my expected output table below.

```python
import pandas as pd
with arcpy.da.SearchCursor(filename, '*') as rows:
    display(pd.DataFrame(data=list(rows), columns=rows.fields))
```

### Hints

1. Check out the Spatial Analyst [Extraction toolset](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/an-overview-of-the-extraction-tools.htm). Carefully read the descriptions on that page of what each tool does and where it puts its output. There are a few that sound very similar but only one that does exactly what I'm asking for. If you choose the correct one, this is pretty straightforward. If you try to use the wrong one, then this problem becomes either extremely difficult or impossible. This is an exercise in finding the right tool for the job!
2. In order to specify the new column names, you'll need a list of lists, where each inner list has a raster name and a column name, like `[(raster1, name1), (raster2, name2)]`. Don't type out the band names (because you want this to work on any raster even if the band names are different). So here's one way to make that list without knowing the band names. Try each step by itself until you know your code works, and then you can combine it all together into one cell if you want.
   1. Get a list of catalog paths for the bands in the raster. The Multiple-band rasters section of the map-algebra notebook shows you how to get a list of bands for a raster, and you can turn this into a list of catalog paths using a list comprehension like `[band.catalogPath for band in bands]`.
   2. Create a list of new column names ('green', 'red', and 'nir').
   3. Use [`zip()`](https://www.geeksforgeeks.org/zip-in-python/) to combine the two lists into a list of lists. You saw `zip()` examples near the end of the for-loops notebook in week 3.
   4. The result from the last step will be a `zip` object, which usually you can use without converting it to a list, but for some reason arcpy doesn't like it. So convert it to a list with `list(zip_object)`.

### Expected output 

The first few rows of the attribute table should have these values:

ID | COVER  | Count | green | red | nir
-- | ------ | ----- | ----- | --- | ---
1  | shrubs | 3     | 46    | 30  | 47
2  | trees  | 4     | 87    | 80  | 77
3  | rocks  | 2     | 68    | 57  | 68
4  | grass  | 3     | 74    | 63  | 74


## Problem 2

Create a notebook called `problem2`. Set up four variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\8-rasters\data'
input_raster = 'aster.img'
output_raster = 'savi.tif'
L = 0.5
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

Have your notebook use the input raster to calculate a [Soil Adjusted Vegetation Index](https://en.wikipedia.org/wiki/Soil-Adjusted_Vegetation_Index) (SAVI) for the raster. SAVI is another vegetation index, like NDVI, but this one attempts to adjust for different soils and the way they reflect data back to the sensor.

Here's the equation:

![savi equation](images/savi-eq.png)

where `NIR` is the near-infrared band (you can assume this is band 3), `Red` is the red band (band 2), and `L` is a correction factor.

1. Create the SAVI from the `input_raster`.
2. Use [Set Null](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/set-null.htm) to set all of the edge pixels (where ***both*** the RED and NIR bands are 0) to NoData. There are no division by zero errors (thanks to the *L* in the denominator), so arcpy doesn't automatically set anything to NoData in this case. (I had to add *something* to make this a little different from the example in the notebook and force you to think more!). **See the hints below.**
3. Save the resulting raster using the `output_raster` filename.
4. Create a new Raster object using the `output_raster` filename. **Use this new raster object for the next steps (NOT the one in memory from the calculation).** The only reason I'm making you do this is to provide proof that you saved your output and it's correct (there is a reason for seemingly dumb things I make you do!). Normally I'd just use whatever was already in memory for the next few steps.
   1. Print out the min, max, mean, and standard deviation values in your SAVI raster. **Make sure that yours match those shown below. Pay special attention to the mean and standard deviation.**
   2. Plot your SAVI raster using matplotlib and a two standard deviation stretch and masking the NoData values. I'm having you do it this way because the easy way might not work for some people, and you should all have to do the same thing (plus, standardization makes things easier for grading).  Please use the gray color scheme so that I can tell at a glance if it looks how it's supposed to. But feel free to include a second plot with a scheme you like better if you want.

### Hints

This problem is very similar to the NDVI example in the map-algebra notebook, except that the equation is different.

There are multiple ways to set the edges to null, but here's one method. The first parameter to `SetNull()` is a raster that determines which pixels in the output are null. If a pixel in this raster is True, then the output raster will have No Data for that pixel. The second parameter to `SetNull()` is another raster that specifies output pixel values for the pixels where the first raster is False. So for example, if for some crazy reason you wanted to set NDVI pixels to No Data in the pixels where the red band had a value of 50, you could do it like this:

```python
result = arcpy.sa.SetNull(red == 50, ndvi)
```

The first parameter is a raster that's True everywhere that the red raster is 50, and False everywhere else. The output `result` raster will have No Data in the pixels where red is 50, and all other pixels values will be copied from the original ndvi raster. 

You can also use multiple conditions, so if you wanted to set null where red was equal to 50 or greater than 200, you could use `(red == 50) | (red > 200)`. The `|` means `or`, and the parentheses around each part are required. If you wanted to set null where red was equal to 50 and green was greater than 200, you could do `(red == 50) & (green > 200)`. This time the `&` means `and`. 

So what condition(s) do you need to use for this problem?



### Expected output 

Statistic          | Value
------------------ | ------------------
Min                | -1.496021270752
Max                |  1.4942084550858
Mean               |  0.077014790752435
Standard deviation |  0.23773409873184

The plot will look something like this:

![savi](images/savi.png)


## Problem 3

Create a notebook called `problem3`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer (make sure that `kernel_filename` is pointing to your 8-rasters folder, not your data folder, so that GitHub Desktop can find it):

```python
input_raster = r'D:\classes\NR6920\Assignments\8-rasters\data\aster.img\ASTER_Band1'
output_raster = r'D:\classes\NR6920\Assignments\8-rasters\data\sharpened_aster.tif'
kernel_filename = r'D:\classes\NR6920\Assignments\8-rasters\sharpen.txt'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

Because you're using full paths to the files, so there's no need to set the arcpy workspace. You don't need to use classtools, either, so you don't need a folder to set that up in ArcGIS.

1. Have your script run a focal statistics analysis using the input raster and kernel file (see the hints below for information on creating this) and save the result using the output name. Notice that `input_raster` contains a band name at the end. That's so that your focal statistics will run on a single band in the raster instead of all three of them (depending on your version of ArcGIS).
2. Create a new raster object using the output filename. Use this for these steps:
   1. Print out the min, max, mean, and standard deviation values in the new raster.
   2. Plot the new raster using a two standard deviation stretch and gray colormap. You'll also need to add another parameter to `imshow()` that isn't in the notebooks or else you'll just see a mass of gray. So also include `interpolation='none'` to tell it not to interpolate pixels values and wash things out.
   3. The plot is pretty ugly zoomed out, so add a second plot that's zoomed in and has a `figsize` of `(10,10)` (see the map algebra notebook for how to set that). You can use the same stretch values that you used for the entire raster, and use the `interpolation` parameter again, as well. To zoom in, use this code to read the raster into a numpy array for plotting (change `result` to **your** variable name):

```python
arcpy.RasterToNumPyArray(result, arcpy.Point(466000, 4628500), 800, 500)
```

### Hints

Once you make your kernel file, this problem is just like the kernel file example in the moving-windows notebook. Make sure you use the correct `statistics_type` for this sort of neighborhood!

You will need to create the kernel file, called `sharpen.txt` (use this filename so that my tests can find it). Notice that it's not in the data folder-- GitHub Desktop won't see it if it's in there, which will make it harder to turn in. You can create this file in any text editor, including Jupyter. New text files are in the same menu as new notebooks. The format of kernel files is shown in the moving-windows notebook, and here are the weights you'll need:

<table style="width:200px;">
<tr><td>-1</td><td>-1</td><td>-1</td></tr>
<tr><td>-1</td><td> 8</td><td>-1</td></tr>
<tr><td>-1</td><td>-1</td><td>-1</td></tr>
</table>

**Make sure you turn the kernel file in along with your notebook.**

### Expected output 

Statistic          | Value
------------------ | ------------------
Min                | -734.0
Max                |  799.0
Mean               | -2.4387200221458e-17 (which is essentially 0)
Standard deviation |  14.610226688671

![sharpened](images/sharpen.png)
