{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 8-rasters\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\8-rasters\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import arcpy\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True\n",
    "\n",
    "arcpy.CheckOutExtension('spatial')\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If you're running this in ArcGIS Pro\n",
    "\n",
    "Make sure the Random map is the active map because it has the data used in this notebook (the random.tif raster created in the first notebook)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Moving windows\n",
    "\n",
    "Moving windows are a type of [neighborhood](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/an-overview-of-the-neighborhood-tools.htm) analysis in ArcGIS. Instead of calculating output by using the same pixel location in multiple rasters, the output is based on surrounding pixels in the same (usually) raster. If this doesn't make sense, read the pages I've linked to in the Esri documentation, because they go into it in more detail.\n",
    "\n",
    "Here's an example of a 3x3 majority moving window, where the raster on the left is the input and the one on the right is the output. The value of each output pixel is the most common value in the nine input pixels that make up a 3x3 grid surrounding the pixel of interest. The thicker outlines over two areas show two different neighborhoods that make up the calculation for the single output pixels that also have thicker outlines.\n",
    "\n",
    "The region on the right has a white (No Data) output because there is no majority in its input region.\n",
    "\n",
    "![majority](images/majority.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ArcGIS has several different kinds of [neighborhoods](https://pro.arcgis.com/en/pro-app/arcpy/spatial-analyst/an-overview-of-neighborhood-classes.htm) that can be used for moving windows. Let's use a square one to smooth a dataset by averaging the pixels in a 5x5 window. You'll use the [Focal Statistics](https://pro.arcgis.com/en/pro-app/tool-reference/image-analyst/focal-statistics.htm) tool for this. The syntax is \n",
    "\n",
    "```\n",
    "FocalStatistics(in_raster, {neighborhood}, {statistics_type}, {ignore_nodata}, {percentile_value})\n",
    "```\n",
    "\n",
    "You need to create a [NbrRectangle](https://pro.arcgis.com/en/pro-app/arcpy/spatial-analyst/nbrrectangle-class.htm) object to use as the `neighborhood` parameter.\n",
    "\n",
    "```\n",
    "NbrRectangle({width}, {height}, {units})\n",
    "```\n",
    "\n",
    "The width and height are self-explanatory, and units is either 'CELL' or 'MAP'. CELL means that the width and height are the number of pixels, and MAP means that they're in map units (e.g. meters).\n",
    "\n",
    "*This example uses random.tif created in the rasters-intro notebook.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a rectangular neighborhood that's 5x5 cells in size.\n",
    "nbrhood = arcpy.sa.NbrRectangle(width=5, height=5, units='cell')\n",
    "\n",
    "# Use that neighborhood to take the mean of each 5x5 window in \n",
    "# random.tif and use it to create a smoothed raster.\n",
    "smoothed = arcpy.sa.FocalStatistics(in_raster='random.tif', neighborhood=nbrhood, statistics_type='mean')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now check out the difference between the two rasters. You don't need to worry about how this is working unless you want to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll start reading data at the 100th row and 100th column in the sample rasters.\n",
    "corner = arcpy.Point(100, 100)\n",
    "\n",
    "# Create two small plots side by side.\n",
    "fig, ax = plt.subplots(1, 2, figsize=(10,5))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the original random raster, starting at\n",
    "# the 100th row and column, and plot it on the first subplot.\n",
    "ax[0].set_title('Original')\n",
    "ax[0].imshow(arcpy.RasterToNumPyArray(in_raster='random.tif', lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the smoothed random raster, starting at\n",
    "# the 100th row and column, and plot it on the second subplot.\n",
    "ax[1].set_title('Smoothed')\n",
    "ax[1].imshow(arcpy.RasterToNumPyArray(in_raster=smoothed, lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Force ArcGIS to show the figure.\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*If you're interested in how that plot was done, the `corner` variable is the lower left corner to start reading when the raster is imported into a numpy array, and you're importing 20 columns and 20 rows. The `subplot()` code tells matplotlib that you want to draw a grid of plots with one row and two columns, with `subplot(rows, cols, i)` where `i` is the index of the plot in the grid to draw the next request in. So you're selecting plot 1 and drawing the original random raster and then selecting plot 2 and drawing the smoothed one.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual, there's more than one way to accomplish this. Let's think about it in another way. The average of the 25 pixels (from the 5x5 window) is their sum divided by 25, right? If you think back to your algebra days, you'll remember that\n",
    "\n",
    "\\begin{equation*}\n",
    "\\frac{n_1 + n_2 + n_3 + ... + n_{25}}{25} = \\frac{1}{25}n_1 + \\frac{1}{25}n_2 + \\frac{1}{25}n_3 + ... + \\frac{1}{25}n_{25} = 0.04n_1 + 0.04n_2 + 0.04n_3 + ... + 0.04n_{25}\n",
    "\\end{equation*}\n",
    "\n",
    "So taking the average is equivalent to multiplying each of the 25 input values by a weight, in this case 0.04 for each of them. You can plug this info into what ArcGIS calls a *kernel* file and use that with the `FocalStatistics` tool to get the same result. There's a kernel file called `5x5.txt` in your data folder. It's a plain text file (so you can open it in Jupyter or Notepad), and the contents look like this:\n",
    "\n",
    "```\n",
    "5 5\n",
    "0.04 0.04 0.04 0.04 0.04\n",
    "0.04 0.04 0.04 0.04 0.04\n",
    "0.04 0.04 0.04 0.04 0.04\n",
    "0.04 0.04 0.04 0.04 0.04\n",
    "0.04 0.04 0.04 0.04 0.04\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you look at the first line in the file, the first 5 is the number of columns in the window and the second is the number of rows. The rest of the numbers are the weights for each pixel.\n",
    "\n",
    "You use this file to create a [NbrWeight](https://pro.arcgis.com/en/pro-app/arcpy/spatial-analyst/nbrweight-class.htm) object. For some insane reason, this doesn't honor the ArcPy workspace or the Python working directory so you have to provide the full path. If you don't, it won't spit out any errors but will use its default neighborhood instead, which is almost certainly not what you want if you're bothering to make a kernel file. It would be nice if it warned you, but it doesn't (ArcMap would crash, which is actually a better thing to do, since you *know* it's not using your file in that case)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a full path to the file by joining the data folder and filename together.\n",
    "fn = os.path.join(arcpy.env.workspace, '5x5.txt')\n",
    "\n",
    "# Create a weighted neighborhood from the file.\n",
    "nbrhood = arcpy.sa.NbrWeight(inKernelFile=fn)\n",
    "\n",
    "# Print out the filename that it will use to make the kernel,\n",
    "# just to make sure it's correct. It should include the ENTIRE path.\n",
    "print(nbrhood.inKernelFile)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use the weighted neighborhood with the Focal Statistics tool, but this time you want to **use a statistics type of `'SUM'` instead of `'MEAN'`**, because you want to multiply every pixel in the neighborhood by the weight shown in the file, and then sum them all up (refer to the equation above)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Smooth the random raster using the weighted neighborhood and sum.\n",
    "smoothed2 = arcpy.sa.FocalStatistics(in_raster='random.tif', neighborhood=nbrhood, statistics_type='sum')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And let's see if it looks like the previously smoothed raster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll start reading data at the 100th row and 100th column in the sample rasters.\n",
    "corner = arcpy.Point(100, 100)\n",
    "\n",
    "# Create two small plots side by side.\n",
    "fig, ax = plt.subplots(1, 2, figsize=(10,5))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the original random raster, starting at\n",
    "# the 100th row and column, and plot it on the first subplot.\n",
    "ax[0].set_title('Smoothed original')\n",
    "ax[0].imshow(arcpy.RasterToNumPyArray(in_raster=smoothed, lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the smoothed random raster, starting at\n",
    "# the 100th row and column, and plot it on the second subplot.\n",
    "ax[1].set_title('Smoothed new')\n",
    "ax[1].imshow(arcpy.RasterToNumPyArray(in_raster=smoothed2, lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Force ArcGIS to show the figure.\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They should look the same, since they're just two methods of doing the exact same thing.\n",
    "\n",
    "You can use this technique to run different types of smoothing algorithms that aren't as simple as a mean value. For example, say you wanted to ignore the very middle pixel in the calculation, and instead use the average of the remaining 24. You could change the kernel file to look like this (the weights are changed to reflect 1/24 instead of 1/25):\n",
    "\n",
    "Contents of `5x5-2.txt`:\n",
    "\n",
    "```\n",
    "5 5\n",
    "0.0417 0.0417 0.0417 0.0417 0.0417\n",
    "0.0417 0.0417 0.0417 0.0417 0.0417\n",
    "0.0417 0.0417 0.0    0.0417 0.0417\n",
    "0.0417 0.0417 0.0417 0.0417 0.0417\n",
    "0.0417 0.0417 0.0417 0.0417 0.0417\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Run a moving window analysis that calculates the mean of the 24 surrounding pixels, but doesn't include the target pixel. This will be exactly like the previous kernel file example except that it uses the `5x5-2.txt` kernel file instead. Call your output `smoothed3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "Now run this cell to see the difference between the moving window analysis that uses 25 pixels and the one that uses 24 pixels. They'll probably look very similar, but shouldn't look exactly the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "# We'll start reading data at the 100th row and 100th column in the sample rasters.\n",
    "corner = arcpy.Point(100, 100)\n",
    "\n",
    "# Create two small plots side by side.\n",
    "fig, ax = plt.subplots(1, 2, figsize=(10,5))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the original random raster, starting at\n",
    "# the 100th row and column, and plot it on the first subplot.\n",
    "ax[0].set_title('Smoothed with 5x5.txt')\n",
    "ax[0].imshow(arcpy.RasterToNumPyArray(in_raster=smoothed2, lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Read in 20 rows and 20 columns from the smoothed random raster, starting at\n",
    "# the 100th row and column, and plot it on the second subplot.\n",
    "ax[1].set_title('Smoothed with 5x5-2.txt')\n",
    "ax[1].imshow(arcpy.RasterToNumPyArray(in_raster=smoothed3, lower_left_corner=corner, ncols=20, nrows=20))\n",
    "\n",
    "# Force ArcGIS to show the figure.\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "304px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
